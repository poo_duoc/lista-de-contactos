/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package listaContactos;

/**
 *
 * poo2201
 */
public class Lista {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {     
        
        Contacto agenda;
        agenda = new Contacto();
        
        Persona persona1 = new Persona();
        persona1.setNombre("Juan");
        
        Persona persona2 = new Persona(15, "Luis", "", "");
        Persona persona3 = new Persona(15, "Luis", "", "");
        
        agenda.agregarPersona(persona1);
        agenda.agregarPersona(persona2);
        agenda.agregarPersona(persona3);
        agenda.agregarPersona(new Persona(20, "Felipe", "", ""));
        agenda.agregarPersona(new Persona(20, "Felipe", "", ""));
        agenda.listar();
        agenda.eliminarPersona(persona1);
        System.out.println("--------------------");
        agenda.listar();
        
        
        
    }
}

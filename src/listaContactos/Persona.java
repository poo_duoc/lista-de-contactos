/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package listaContactos;

/**
 *
 * poo2201
 */
public class Persona {

    //atributos
    private int numero;
    private String nombre;
    private String direccion;
    private String correo;

    //constructores
    public Persona() {
    }

    public Persona(int numero, String nombre, String direccion, String correo) {
        this.numero = numero;
        this.nombre = nombre;
        this.direccion = direccion;
        this.correo = correo;
    }

    //accesadores y mutadores
    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    @Override
    public String toString() {
        return "Persona{" + "numero=" + numero + ", nombre=" + nombre + ", direccion=" + direccion + ", correo=" + correo + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Persona) {
            Persona aux = (Persona) o;

            if (aux.getNombre().equals(this.getNombre()) &&
                    aux.getNumero() == this.getNumero()) {
                return true;
            } else {
                return false;
            }
        } else {
            System.out.println("Tipo incorrecto");
            return false;
        }
    }
}

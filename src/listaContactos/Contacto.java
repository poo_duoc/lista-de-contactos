/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package listaContactos;

import java.util.ArrayList;

/**
 *
 * poo2201
 */
public class Contacto {
    //declarar colección personas que almacena objetos 
    //de la clase Persona
    private ArrayList<Persona> listaContactos;

    //crear colección
    public Contacto() {
        listaContactos = new ArrayList<Persona>();
    }

    //agregar Persona a la colección sino existe su número
    public boolean agregarPersona(Persona nuevaPersona) {
        if (this.verificarNumeroPersona(nuevaPersona.getNumero()) == false){
            listaContactos.add(nuevaPersona);
            return true;
        } else {
            System.out.println("Persona ya existe");
            return false;
        }
    }

    //mostrar Personas almacenadas en la colección
    public void listar() {
        for (Persona temporalPersonas : listaContactos) {
            System.out.println(temporalPersonas.toString());
        }
    }

    //método que devuelve true si el número de la Persona se encuentra
    //en la colección sino retorna false
    public boolean verificarNumeroPersona(int numero) {
        boolean valida = false;
        for (Persona temporalPersonas : listaContactos) {
            if (temporalPersonas.getNumero() == numero) {
                valida = true;
                break;
            }
        }
        return valida;
    }

    //eliminar contacto
    public void eliminarPersona(Persona personaBorrar) {
        for (int i = 0; i < listaContactos.size(); i++) {
            if (listaContactos.get(i).equals(personaBorrar)) {
                listaContactos.remove(i);
                i = i - 1;
            }
        }
    }
    
    //obtener contactos que empiezan con alguna letra
    public int obtenerTotalLetra(char letra){
        int contador = 0;
        for (Persona temporalPersonas : listaContactos) {
            if(temporalPersonas.getNombre().charAt(0) == letra){
                contador = contador + 1; 
            }
        }
        return contador;
    }
}
